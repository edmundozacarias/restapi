Feature: Lista productos
    Scenario: Cargar lista de productos
        When we request the products list
        Then we should receive
            | nombre | descripcion |
            | Movil XL | Telefono grande con una de las mejores pantallas. |
            | Movil Mini | Telefono mediano con una de las mejores camaras. |
            | Movil Standard | Telefono estandar, nada especial. |