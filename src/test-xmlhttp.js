import { LitElement, html } from 'lit-element';

export class TestXmlhttp extends LitElement {

    static get properties() {
        return {
            planet: { type: Object }
        };
    }

    constructor() {
        super();
        this.cargarPlaneta();
    }

    render() {
        return html`
        <p><code>${this.planet}</code></p>
        `;
    }

    cargarPlaneta() {
        //xmlHttpRequets
        var req = new XMLHttpRequest();
        req.open('GET', "https://swapi.dev/api/planets/1", true);
        req.onreadystatechange =
            ((aEvt) => {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        this.planet = req.responseText;
                        this.requestUpdate();
                    } else {
                        alert("Error llamado a REST");
                    }
                }
            });

        req.send(null);
    }
}
customElements.define('test-xmlhttp', TestXmlhttp);